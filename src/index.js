import React from "react";
import ReactDOM from "react-dom";

const App = function () {
  return (
    <div>
      <DayTitle></DayTitle>
      <Days></Days>
      <MonthsTitle></MonthsTitle>
      <Months></Months>
      <ZodiacTitle></ZodiacTitle>
      <Zodiac></Zodiac>
    </div>
  );
};
// Дні тижня
const daysArr = [
  "Понеділок",
  "Вівторок",
  "Середа",
  "Четвер",
  "П'ятниця",
  "Субота",
  "Неділя",
];
const day = daysArr.map((day) => <li key={day}>{day}</li>);

const Days = function () {
  return <ul>{day}</ul>;
};
const DayTitle = function () {
  return <h2>Дні тижня</h2>;
};
// Місяці
const monthsArr = [
  "Січень",
  "Лютий",
  "Березень",
  "Квітень",
  "Травень",
  "Червень",
  "Липень",
  "Серпень",
  "Вересень",
  "Жовтень",
  "Листопад",
  "Грудень",
];
const month = monthsArr.map((month) => <li key={month}>{month}</li>);
const Months = function () {
  return <ul>{month}</ul>;
};

const MonthsTitle = function () {
  return <h2>Місяці</h2>;
};

// Знаки зодіаку
const zodiacArr = [
  "Овен",
  "Тілець",
  "Близнюки",
  "Рак",
  "Лев",
  "Діва",
  "Терези",
  "Скорпіон",
  "Стрілець",
  "Козеріг",
  "Водолій",
  "Риби",
];
const zodiacItem = zodiacArr.map((zodiacItem) => (
  <li key={zodiacItem}>{zodiacItem}</li>
));
const Zodiac = function () {
  return <ul>{zodiacItem}</ul>;
};

const ZodiacTitle = function () {
  return <h2>Знаки Зодіаку</h2>;
};

ReactDOM.render(<App></App>, document.querySelector("#root"));
